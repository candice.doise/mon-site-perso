---
title: Contact
body_classes: modular
form:
    name: contact

    fields:
        name:
          placeholder: Nom
          autocomplete: on
          type: text
          validate:
            required: true

        email:
          placeholder: Email
          type: email
          validate:
            required: true

        message:
          placeholder: Message
          type: textarea
          validate:
            required: true

        g-recaptcha-response:
          label: Captcha
          type: captcha
          recaptcha_not_validated: 'Captcha not valid!'

    buttons:
        submit:
          type: submit
          value: Envoyer
          class: valider


    process:
        save:
            fileprefix: contact-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        email:
          from: "{{ config.plugins.email.from }}"
          to: "{{ config.plugins.email.to }}"
          subject: "Contact by {{ form.value.name|e }}"
          body: "{% include 'forms/data.html.twig' %}"
        captcha: true
       
---

