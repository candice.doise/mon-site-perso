---
title: 'Animation d''automne'
color: '#9C0F48'
image: candice.gif
---


Cette animation a été réalisée pour un exercice lors de mon apprentissage du logiciel Animate. Tout a été dessiné à la main sur procreate. Mon but était de mettre en avant cette saison que je trouve particulièrement représentative du temps qui passe.